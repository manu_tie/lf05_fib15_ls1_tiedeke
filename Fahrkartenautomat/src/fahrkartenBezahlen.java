import java.util.Scanner;
public class fahrkartenBezahlen {
	double  zuZahlenderBetrag; 
	double eingezahlterGesamtbetrag;
	double eingeworfeneM�nze;
	double r�ckgabebetrag;
	Scanner tastatur;
	public fahrkartenBezahlen(double zuZahlenderBetrag, Scanner in) {
		this.zuZahlenderBetrag=zuZahlenderBetrag; 
		tastatur=in;
	}

	
	public double bezahlen() {

		
		eingezahlterGesamtbetrag = 0.00;
	   while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	   {  
		   System.out.printf("%s %.2f %s","Noch zu zahlen: " ,(zuZahlenderBetrag - eingezahlterGesamtbetrag)," Euro \n");
		   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
		   
		   eingeworfeneM�nze =tastatur.nextDouble();
	       eingezahlterGesamtbetrag += eingeworfeneM�nze;
	       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	   }
	   
	  
	   return r�ckgabebetrag;
	}
	
}
