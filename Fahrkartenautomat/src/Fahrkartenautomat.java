﻿import java.util.Scanner;
class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	Scanner tastatur = new Scanner(System.in);
    	
    	fahrkartenbestellungErfassen bestellung=new fahrkartenbestellungErfassen(tastatur);
    	fahrkartenBezahlen bezahlen =new fahrkartenBezahlen(bestellung.fahrkartenbesetellung(), tastatur);
    	fahrkartenAusgeben ausgabe= new fahrkartenAusgeben();
    	rueckgeldAusgeben geld= new rueckgeldAusgeben(bezahlen.bezahlen());
    	ausgabe.ausgabe();
    	geld.rückgeld();
    	tastatur.close();
   
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}