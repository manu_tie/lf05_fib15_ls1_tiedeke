
public class rueckgeldAusgeben {
	double r�ckgabebetrag;
	int betrag;
	String einheit;
	public rueckgeldAusgeben(double r�ckgabebetrag) {
		this.r�ckgabebetrag= r�ckgabebetrag;
	}
	
	public void r�ckgeld() {
		 if(r�ckgabebetrag > 0.0)
		   {
			   System.out.printf("%s %.2f %s","Der R�ckgabebetrag in H�he von ",r�ckgabebetrag ," EURO \n");
			   System.out.println("wird in folgenden M�nzen ausgezahlt:");

		       while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
		       {
		    	   betrag=2;
		    	   einheit="EURO";
		    	  muenzeAusgeben(betrag, einheit);
		          r�ckgabebetrag -= 2.0;
		       }
		       while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
		       {
		    	   betrag=1;
		    	   einheit="EURO";
		    	  muenzeAusgeben(betrag, einheit);
		          r�ckgabebetrag -= 1.0;
		       }
		       while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
		       {
		    	   betrag=50;
		    	   einheit="CENT";
		    	  muenzeAusgeben(betrag, einheit);
		          r�ckgabebetrag -= 0.5;
		       }
		       while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
		       {
		    	   betrag=20;
		    	   einheit="CENT";
		    	  muenzeAusgeben(betrag, einheit);
		          r�ckgabebetrag -= 0.2;
		       }
		       while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
		       {
		    	   betrag=10;
		    	   einheit="CENT";
		    	  muenzeAusgeben(betrag, einheit);
		          r�ckgabebetrag -= 0.1;
		       }
		       while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
		       {
		    	   betrag=5;
		    	   einheit="CENT";
		    	  muenzeAusgeben(betrag, einheit);
		          r�ckgabebetrag -= 0.05;
		       }
		   }
	}
	
	 static void muenzeAusgeben(int betrag, String einheit) {
		 System.out.println(betrag+" "+einheit);
	 }
}
